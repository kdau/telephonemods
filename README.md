# Telephone Mods

Telephone Framework and Universal Service are [Stardew Valley](http://stardewvalley.net/) mods that extend the game's Telephone object.

Both mods are made by kdau and offered under an [MIT license](LICENSE). Their source can be found on [GitLab](https://gitlab.com/kdau/telephonemods). The [issue tracker](https://gitlab.com/kdau/telephonemods/-/issues) covers bug reports and feature plans.

## Mods in this repository

* ![[icon]](TelephoneFramework/promo/icon.png) **[Telephone Framework](TelephoneFramework)**

	Reach out and touch Stardew Valley. Add inbound and outbound calls for the Telephone object with content packs or an API for SMAPI mods.

* ![[icon]](UniversalService/promo/icon.png) **[Universal Service](UniversalService)**

	Phones for everybody! Call the Traveling Merchant, Krobus, Sandy and more, plus receive some surprising calls on occasion.

## [My other Stardew stuff](https://www.kdau.com/stardew)
