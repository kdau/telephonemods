using StardewModdingAPI;
using StardewModdingAPI.Events;

namespace UniversalService
{
	public class ModEntry : Mod
	{
		internal static ModEntry Instance { get; private set; }

		public override void Entry (IModHelper helper)
		{
			// Make resources available.
			Instance = this;
			ModConfig.Load ();

			// Handle game events.
			Helper.Events.GameLoop.GameLaunched += onGameLaunched;

			// TODO: outbound calls via API:
			// - Harvey's Clinic (Harvey or Maru; hours; whether there is an appointment that day)
			// - JojaMart (Morris; hours and inventory; membership and community development options)
			// - Travelling Merchant (if visited once [how to track?]; hours and inventory on Fri, Sun and Night Market days; otherwise OOO)
			// TODO: inbound calls via content pack:
			// - Dwarf (untranslated, before having DTG, nighttime)
			// - Harvey (medical reminders)
			// - Leo (if moved to Valley; parrot noises or something)
			// TODO: inbound calls via API:
			// - Demetrius (latest info from mushrooms or fruit bats, once present)
			// - Gil (monster eradication suggestions)
			// - Gunther (museum donation suggestions, once introduced and until collection complete)
			// - Mr. Qi (inspirational quotes and/or player statistics)
			// - current island visitor(s) calling from vacation?
		}

		private void onGameLaunched (object _sender, GameLaunchedEventArgs _e)
		{
			// Set up Generic Mod Config Menu, if it is available.
			ModConfig.SetUpMenu ();
		}
	}
}
