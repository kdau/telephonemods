![[icon]](https://www.kdau.com/UniversalService/icon.png) Phones for everybody! Call the Traveling Merchant, Krobus, Sandy and more, plus receive some surprising calls on occasion.

## ![Compatibility](https://www.kdau.com/headers/compatibility.png)

**Game:** Stardew Valley 1.5+

**Platform:** Linux, macOS or Windows

**Multiplayer:** works; every player must install

**Other mods:** no known conflicts

## ![Installation](https://www.kdau.com/headers/installation.png)

1. Install [SMAPI](https://smapi.io/)
1. Install [PlatoTK](https://www.nexusmods.com/stardewvalley/mods/6589) (This is different from PyTK!)
1. Install [Shop Tile Framework](https://www.moddrop.com/stardew-valley/mods/716384-shop-tile-framework)
1. Install [Telephone Framework](https://www.moddrop.com/stardew-valley/mods/TODO)
1. Install [Generic Mod Config Menu](https://www.moddrop.com/stardew-valley/mods/771692-generic-mod-config-menu) (optional, for easier configuration)
1. Download this mod from the link in the header above
1. Unzip and put the `UniversalService` and `UniversalService.TF` folders inside your `Mods` folder
1. Run the game using SMAPI

## ![Use](https://www.kdau.com/headers/use.png)

TODO: ...

## ![[Configuration]](https://www.kdau.com/headers/configuration.png)

If you have installed Generic Mod Config Menu, you can access this mod's configuration by clicking the cogwheel button at the lower left corner of the Stardew Valley title screen and then choosing "Universal Service".

Otherwise, you can edit this mod's `config.json` file. It will be created in the mod's main folder (`Mods/UniversalService`) the first time you run the game with the mod installed. These options are available:

* TODO: ...

## ![Translation](https://www.kdau.com/headers/translation.png)

No translations are available yet.

This mod can be translated into any language supported by the game. Your contribution would be welcome. Please see the [instructions on the wiki](https://stardewvalleywiki.com/Modding:Translations). Also note that there are two i18n folders: `UniversalService/i18n` and `UniversalService.TF/i18n`. You can send me your work in [a GitLab issue](https://gitlab.com/kdau/telephonemods/-/issues) or the Comments tab above.

## ![Acknowledgments](https://www.kdau.com/headers/acknowledgments.png)

* Like all mods, this one is indebted to ConcernedApe, Pathoschild and the various framework modders.
* The #making-mods channel on the [Stardew Valley Discord](https://discord.gg/StardewValley) offered valuable guidance and feedback.

## ![See also](https://www.kdau.com/headers/see-also.png)

* [Release notes](https://gitlab.com/kdau/telephonemods/-/blob/main/UniversalService/doc/RELEASE-NOTES.md)
* [Source code](https://gitlab.com/kdau/telephonemods/-/tree/main/UniversalService)
* [Report bugs](https://gitlab.com/kdau/telephonemods/-/issues)
* [My other Stardew stuff](https://www.kdau.com/stardew)
* Mirrors:
	[Nexus](https://www.nexusmods.com/stardewvalley/mods/TODO),
	**ModDrop**,
	[forums](https://forums.stardewvalley.net/resources/TODO/)

Other mods to consider:

* ![[icon]](https://www.kdau.com/PublicAccessTV/icon.png) [Public Access TV](https://www.moddrop.com/stardew-valley/mods/757967-public-access-tv) to hear from your neighbors on TV
