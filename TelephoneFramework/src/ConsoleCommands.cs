using StardewModdingAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TelephoneFramework
{
	internal static class ConsoleCommands
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		// private static ModConfig Config => ModConfig.Instance;
		private static Api Api => ModEntry.Instance.api;

		public static void OnEntry ()
		{
			Helper.ConsoleCommands.Add ("telephone_list_outbound",
				"Lists all possible outbound Telephone calls on the console. Starred calls are available in the current context.",
				ListOutbound);
			Helper.ConsoleCommands.Add ("telephone_list_inbound",
				"Lists all possible inbound Telephone calls on the console. Starred calls are available in the current context.",
				ListInbound);

			Helper.ConsoleCommands.Add ("telephone_call",
				"Places an outbound Telephone call, without using the object itself.\n\nUsage: telephone_call [S:key] [B:ignoreConditions]\n- key: key of the outbound call to dial (if omitted, shows the menu of calls)\n- ignoreConditions: whether to ignore conditions on the availability of calls (default true if key given, false if not)",
				Call);
			Helper.ConsoleCommands.Add ("telephone_ring",
				"Causes an inbound Telephone call to ring for all players, if none is ringing already. Can only be used by the farmer.\n\nUsage: telephone_ring [S:key] [B:ignoreConditions]\n- key: key of the inbound call to ring (if omitted, chooses a call randomly)\n- ignoreConditions: whether to ignore global conditions on the availability of calls (default true if key given, false if not)",
				Ring);
			Helper.ConsoleCommands.Add ("telephone_answer",
				"Answers any currently ringing inbound Telephone call, without using the object itself.\n\nUsage: telephone_answer [B:ignoreConditions]\n- ignoreConditions: whether to ignore player conditions on the availability of the call (default true)",
				Answer);

			Helper.ConsoleCommands.Add ("telephone_reload_packs",
				"Reloads content packs for Telephone Framework from files.",
				ReloadPacks);
		}

		private static void ListOutbound (string _command, string[] _args)
		{
			try
			{
				var allCalls = OutboundCalling.ListCalls (true);
				var availCalls = OutboundCalling.ListCalls (false);
				var baseCalls = Enum.GetNames (typeof (BaseOutboundCall));

				Monitor.Log ("---------------------------------------------------------------------------", LogLevel.Info);
				Monitor.Log ("| Avail | Key                            | Origin                         |", LogLevel.Info);
				Monitor.Log ("---------------------------------------------------------------------------", LogLevel.Info);
				foreach (var call in allCalls)
				{
					if (call.responseKey == "HangUp")
						continue;
					var avail = availCalls.Any ((avail) => call.responseKey == avail.responseKey)
						? "*"
						: " ";
					var key = call.responseKey;
					var origin = baseCalls.Contains (key)
						? "base game"
						: "?";
					var match = Regex.Match (key, @"^([^/]+)/([^/]+)$");
					if (match.Success)
					{
						key = match.Groups[1].Value;
						origin = match.Groups[2].Value;
					}
					Monitor.Log (string.Format ("|   {0}   | {1,-30} | {2,-30} |", avail, key, origin), LogLevel.Info);
				}
				Monitor.Log ("---------------------------------------------------------------------------", LogLevel.Info);
			}
			catch (Exception e)
			{
				Monitor.Log ($"telephone_list_outbound failed: {e.Message}", LogLevel.Error);
			}
		}

		private static void ListInbound (string _command, string[] _args)
		{
			try
			{
				var allCalls = InboundCalling.ListCalls (true);
				var availCalls = InboundCalling.ListCalls (false);

				Monitor.Log ("---------------------------------------------------------------------------", LogLevel.Info);
				Monitor.Log ("| Avail | Key                            | Origin                         |", LogLevel.Info);
				Monitor.Log ("---------------------------------------------------------------------------", LogLevel.Info);
				foreach (var call in allCalls)
				{
					var avail = availCalls.Any ((avail) => call.key == avail.key)
						? "*"
						: " ";
					var key = (call.origin != null)
						? Regex.Replace (call.key, $"^{Regex.Escape (call.origin)}/", "")
						: call.key;
					var origin = call.origin ?? "base game";
					Monitor.Log (string.Format ("|   {0}   | {1,-30} | {2,-30} |", avail, key, origin), LogLevel.Info);
				}
				Monitor.Log ("---------------------------------------------------------------------------", LogLevel.Info);
			}
			catch (Exception e)
			{
				Monitor.Log ($"telephone_list_inbound failed: {e.Message}", LogLevel.Error);
			}
		}

		private static void Call (string _command, string[] args)
		{
			try
			{
				Queue<string> argq = new (args);

				string key = null;
				if (argq.Count > 0 && !bool.TryParse (argq.Peek (), out bool _))
					key = argq.Dequeue ();

				bool ignoreConditions = key != null;
				if (argq.Count > 0)
					bool.TryParse (argq.Dequeue (), out ignoreConditions);

				if (key != null)
					Api.placeOutboundCall (key, ignoreConditions);
				else
					Api.showOutboundMenu (ignoreConditions);
			}
			catch (Exception e)
			{
				Monitor.Log ($"telephone_call failed: {e.Message}", LogLevel.Error);
			}
		}

		private static void Ring (string _command, string[] args)
		{
			try
			{
				Queue<string> argq = new (args);

				string key = null;
				if (argq.Count > 0 && !bool.TryParse (argq.Peek (), out bool _))
					key = argq.Dequeue ();

				bool ignoreConditions = key != null;
				if (argq.Count > 0)
					bool.TryParse (argq.Dequeue (), out ignoreConditions);

				Api.placeInboundCall (key, ignoreConditions);
			}
			catch (Exception e)
			{
				Monitor.Log ($"telephone_ring failed: {e.Message}", LogLevel.Error);
			}
		}

		private static void Answer (string _command, string[] args)
		{
			try
			{
				Queue<string> argq = new (args);

				bool ignoreConditions = true;
				if (argq.Count > 0)
					bool.TryParse (argq.Dequeue (), out ignoreConditions);

				Api.answerInboundCall (ignoreConditions);
			}
			catch (Exception e)
			{
				Monitor.Log ($"telephone_answer failed: {e.Message}", LogLevel.Error);
			}
		}

		private static void ReloadPacks (string _command, string[] _args)
		{
			try
			{
				ContentPacks.Load ();
			}
			catch (Exception e)
			{
				Monitor.Log ($"telephone_reload_packs failed: {e.Message}", LogLevel.Error);
			}
		}
	}
}
