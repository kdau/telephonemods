using PlatoTK;
using PlatoTK.Events;
using StardewModdingAPI;
using StardewModdingAPI.Utilities;
using StardewValley;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TelephoneFramework
{
	internal static class OutboundCalling
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;
		private static IPlatoHelper PlatoHelper => ModEntry.Instance.platoHelper;
		private static Api Api => ModEntry.Instance.api;

		internal static readonly PerScreen<bool> IgnoreConditions =
			new (() => false);
		private static readonly PerScreen<Dictionary<string, PlacedCallback>> PlacedCallbacks =
			new (() => new ());

		public static void OnEntry ()
		{
			Helper.Events.GameLoop.DayStarted += OnDayStarted;
			PlatoHelper.Events.QuestionRaised += OnQuestionRaised;
			PlatoHelper.Events.QuestionAnswered += OnQuestionAnswered;
		}

		internal static IEnumerable<Response> ListCalls (bool ignoreConditions = false)
		{
			if (!Context.IsWorldReady)
				throw new InvalidOperationException ("The world is not ready.");

			// Run the actual menu setup...
			IgnoreConditions.Value = ignoreConditions;
			Game1.game1.ShowTelephoneMenu ();

			// ...but just use the responses...
			var box = Game1.activeClickableMenu as DialogueBox;
			var result = box.responses;

			// ...and tear down the rest.
			box.closeDialogue ();
			IgnoreConditions.Value = false;
			PlacedCallbacks.Value.Clear ();

			return result;
		}

		private static void OnDayStarted (object _sender, EventArgs _e)
		{
			// Just to be sure that nothing lingers.
			IgnoreConditions.Value = false;
			PlacedCallbacks.Value.Clear ();
		}

		private static void OnQuestionRaised (object _sender, IQuestionRaisedEventArgs e)
		{
			// Any previous callbacks are now invalid.
			PlacedCallbacks.Value.Clear ();

			// We only care about one question. Since the "telephone" question
			// key is reused by base game outbound calls for their subsequent
			// IVR menus, we have to check the question text as well.
			if (e.LastQuestionKey == "telephone" && e.Question ==
				Game1.content.LoadString ("Strings\\Characters:Phone_SelectNumber"))
			{
				Monitor.Log ("Preparing outbound call list for Telephone menu",
					Config.VerboseLogging ? LogLevel.Info : LogLevel.Trace);

				// Move the "(Hang up)" choice out of the way for new choices.
				Response hangUp = e.Choices.FirstOrDefault ((r) =>
					r.responseKey == "HangUp");
				if (hangUp != null)
					e.Choices.Remove (hangUp);

				// Let API consumers, including content packs, do their thing.
				Api.dispatchOutboundCallChoosing (IgnoreConditions.Value,
					(IManifest _, string key, string menuText, PlacedCallback placedCallback) =>
					{
						e.Choices.Add (new (key, menuText));
						if (placedCallback != null)
							PlacedCallbacks.Value[key] = placedCallback;
					},
					(IManifest _, string key) =>
					{
						e.Choices.RemoveAll ((r) => r.responseKey == key);
					}
				);

				// Put the "(Hang up)" choice back at the end, changing its
				// text from the default "Cancel".
				if (hangUp != null)
				{
					hangUp.responseText = Game1.content.LoadString ("Strings\\Characters:Phone_HangUp");
					e.Choices.Add (hangUp);
				}

				// Very long lists should be paginated.
				if (e.Choices.Count > 9)
					e.PaginateResponses ();

				// This value is no longer needed.
				IgnoreConditions.Value = false;
			}
		}

		private static void OnQuestionAnswered (object _sender, IQuestionAnsweredEventArgs e)
		{
			// We still only care about one question. This time we don't have
			// access to the question text, but we can proceed anyway. Resetting
			// the context repeatedly is okay, and offering submenu answers to
			// API consumers will allow them to override specific parts of base
			// game calls if desired.
			if (e.LastQuestionKey == "telephone")
			{
				// Hanging up can't be overridden.
				if (e.Answer.responseKey != "HangUp")
				{
					Monitor.Log ($"Handling outbound call to {e.Answer.responseKey}",
						Config.VerboseLogging ? LogLevel.Info : LogLevel.Trace);

					bool prevented = false;

					// Let API consumers, including content packs, do their thing.
					Api.dispatchOutboundCallPlaced (e.Answer.responseKey,
						() =>
						{
							e.PreventDefault ();
							prevented = true;
						}
					);

					// If nothing else took over the answer, see if a callback can.
					if (!prevented &&
						PlacedCallbacks.Value.ContainsKey (e.Answer.responseKey))
					{
						PlacedCallbacks.Value[e.Answer.responseKey] ();
						e.PreventDefault ();
						prevented = true;
					}
				}
			}

			// The context is now invalid.
			IgnoreConditions.Value = false;
			PlacedCallbacks.Value.Clear ();
		}
	}
}
