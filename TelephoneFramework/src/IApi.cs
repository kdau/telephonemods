using StardewModdingAPI;
using System;

namespace TelephoneFramework
{
	// Outbound calling events

	public delegate void PlacedCallback ();

	public interface IOutboundCallChoosingEventArgs
	{
		bool ignoreConditions { get; }
		void addCall (IManifest mod, string key, string menuText,
			PlacedCallback placedCallback = null);
		void removeCall (IManifest mod, string key);
	}

	public interface IOutboundCallPlacedEventArgs
	{
		string callKey { get; }
		void preventDefault ();
	}

	// Inbound calling events

	public delegate void ShouldRingCallback (ref bool shouldRing);
	public delegate void AnsweredCallback ();

	public interface IInboundCallChoosingEventArgs
	{
		bool ignoreConditions { get; }
		void addCall (IManifest mod, string key, uint maxTimes = 1,
			ShouldRingCallback shouldRingCallback = null,
			AnsweredCallback answeredCallback = null);
		void removeCall (IManifest mod, string key);
	}

	public interface IInboundCallRingingEventArgs
	{
		string callKey { get; }
		bool shouldRing { get; set; }
	}

	public interface IInboundCallAnsweredEventArgs
	{
		string callKey { get; }
		void preventDefault ();
	}

	// API interface

	public interface IApi
	{
		// Outbound calling

		event EventHandler<IOutboundCallChoosingEventArgs> outboundCallChoosing;
		event EventHandler<IOutboundCallPlacedEventArgs> outboundCallPlaced;

		void showOutboundMenu (bool ignoreConditions = false);
		void placeOutboundCall (string key, bool ignoreConditions = true);

		void playDialingSequence (string key,
			Action callback = null, bool freezePlayer = true);
		void playDialingSequence (int randomSeed, int count = 7,
			Action callback = null, bool freezePlayer = true);
		void playDialingSequence (int[] digits,
			Action callback = null, bool freezePlayer = true);

		// Inbound calling

		event EventHandler<IInboundCallChoosingEventArgs> inboundCallChoosing;
		event EventHandler<IInboundCallRingingEventArgs> inboundCallRinging;
		event EventHandler<IInboundCallAnsweredEventArgs> inboundCallAnswered;

		void placeInboundCall (string key, bool ignoreConditions = true);
		void answerInboundCall (bool ignoreConditions = true);

		void playHandsetUpDown (Action callback = null, bool freezePlayer = true);
	}
}
