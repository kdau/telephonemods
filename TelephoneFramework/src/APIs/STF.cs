using StardewModdingAPI;
using StardewValley;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Reflection;

#pragma warning disable IDE1006

namespace ShopTileFramework
{
	public interface IApi
	{
		// bool RegisterShops (string dir);
		// bool OpenItemShop (string shopName);
		// bool ResetShopStock (string shopName);
		// Dictionary<ISalable, int[]> GetItemPriceAndStock (string shopName);
	}

	// NOTE: Cherry knows about these reflections:
	// ShopManager.ItemShops
	// ShopManager.AnimalShops
	// ItemShop.DisplayShop (debug: true)
	// AnimalShop.DisplayShop (debug: true)
	// TileUtility.CheckVanillaShop
}

#pragma warning restore IDE1006

namespace TelephoneFramework
{
	internal static class STF
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		// private static IMonitor Monitor => ModEntry.Instance.Monitor;

		private static Assembly TheAssembly;

		public static void OnEntry ()
		{
			Helper.Events.GameLoop.GameLaunched += (_, _) =>
			{
				TheAssembly = Helper.ModRegistry.GetApi ("Cherry.ShopTileFramework")
					?.GetType ()?.Assembly;
			};
		}

		public static bool TryGetShop (string shopName, out IClickableMenu shop)
		{
			shop = null;
			if (TheAssembly == null)
				return false;
			if (TryGetVanillaShop (shopName, out shop))
				return true;
			if (TryGetCustomItemShop (shopName, out shop))
				return true;
			if (TryGetCustomAnimalShop (shopName, out shop))
				return true;
			return false;
		}

		public static bool TryGetVanillaShop (string shopName, out IClickableMenu shop)
		{
			try
			{
				// STF does not recognize this one for opening, although it does
				// recognize "HatMouse" for item modification.
				if (shopName == "Vanilla!HatMouse")
				{
					shop = new ShopMenu (Utility.getHatStock (), 0, "HatMouse");
					return true;
				}

				var TileUtility = TheAssembly.GetType ("ShopTileFramework.Utility.TileUtility");
				var CheckVanillaShop =
					Helper.Reflection.GetMethod (TileUtility, "CheckVanillaShop");
				bool warpingShop = false;
				shop = CheckVanillaShop.Invoke<IClickableMenu> (shopName,
					warpingShop);

				// The STF method doesn't return this shop, so retrieve it.
				if (shopName == "Vanilla!QiShop" && shop == null)
				{
					shop = Game1.activeClickableMenu;
					Game1.activeClickableMenu = null;
				}

				return true;
			}
			catch (Exception)
			{
				shop = null;
				return false;
			}
		}

		public static bool TryGetCustomItemShop (string shopName, out IClickableMenu shop)
		{
			try
			{
				var ShopManager = TheAssembly.GetType ("ShopTileFramework.Shop.ShopManager");
				var ItemShops = Helper.Reflection.GetField<Dictionary<string, object>> (ShopManager, "ItemShops");
				var shopData = ItemShops.GetValue ()[shopName];
				var DisplayShop = Helper.Reflection.GetMethod (shopData, "DisplayShop");
				DisplayShop.Invoke (true); // debug = true to ignore conditions
				shop = Game1.activeClickableMenu;
				return shop != null;
			}
			catch (Exception)
			{
				shop = null;
				return false;
			}
		}

		public static bool TryGetCustomAnimalShop (string shopName,
			out IClickableMenu shop)
		{
			try
			{
				var ShopManager = TheAssembly.GetType ("ShopTileFramework.Shop.ShopManager");
				var AnimalShops = Helper.Reflection.GetField<Dictionary<string, object>> (ShopManager, "AnimalShops");
				var shopData = AnimalShops.GetValue ()[shopName];
				var DisplayShop = Helper.Reflection.GetMethod (shopData, "DisplayShop");
				DisplayShop.Invoke (true); // debug = true to ignore conditions
				shop = Game1.activeClickableMenu;
				return shop != null;
			}
			catch (Exception)
			{
				shop = null;
				return false;
			}
		}
	}
}
