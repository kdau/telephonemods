using StardewValley;
using StardewValley.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TelephoneFramework
{
	internal class InboundCall
	{
		public readonly string key;
		public readonly int which;
		public readonly string origin;
		public uint maxTimes = 1;
		public ShouldRingCallback shouldRingCallback = null;
		public AnsweredCallback answeredCallback = null;

		public InboundCall (string key, int which, string origin)
		{
			this.key = key;
			this.which = which;
			this.origin = origin;
		}

		public bool baseGame => origin == null;

		private string receivedKey => $"kdau.TelephoneFramework.received/{key}";
		public uint receivedCount
		{
			get
			{
				if (baseGame)
				{
					Game1.player.callsReceived.TryGetValue (which, out int value);
					return (uint) Math.Max (0, value);
				}
				else
				{
					Game1.player.modData.TryGetValue (receivedKey, out string stringValue);
					uint.TryParse (stringValue, out uint value);
					return value;
				}
			}
			set
			{
				if (baseGame)
					Game1.player.callsReceived[which] = (int) value;
				else
					Game1.player.modData[receivedKey] = value.ToString ();
			}
		}
	}

	internal class InboundCallingData
	{
		private readonly Dictionary<string, InboundCall> callsByKey = new ();
		private readonly Dictionary<int, InboundCall> callsByWhich = new ();
		private int nextWhich = 7940_0900;

		public InboundCallingData ()
		{
			// Add the base game calls to the list, including time-of-day logic
			// moved from Phone.minutesElapsed.
			foreach (Phone.PhoneCalls call in
				(Phone.PhoneCalls[]) Enum.GetValues (typeof (Phone.PhoneCalls)))
			{
				if (call != Phone.PhoneCalls.NONE && call != Phone.PhoneCalls.MAX)
					addCall (call.ToString (), (int) call, origin: null, maxTimes: 1,
						(ref bool shouldRing) =>
						{
							if (call != Phone.PhoneCalls.Hat)
								shouldRing &= Game1.timeOfDay < 1800;
						});
			}
		}

		public void addCall (string key, string origin, uint maxTimes = 1,
			ShouldRingCallback shouldRingCallback = null,
			AnsweredCallback answeredCallback = null)
		{
			addCall (key, nextWhich++, origin, maxTimes,
				shouldRingCallback, answeredCallback);
		}

		private void addCall (string key, int which, string origin, uint maxTimes = 1,
			ShouldRingCallback shouldRingCallback = null,
			AnsweredCallback answeredCallback = null)
		{
			InboundCall call = new (key, which, origin)
			{
				maxTimes = maxTimes,
				shouldRingCallback = shouldRingCallback,
				answeredCallback = answeredCallback,
			};
			callsByKey[key] = call;
			callsByWhich[which] = call;
		}

		public void removeCall (string key)
		{
			if (callsByKey.TryGetValue (key, out InboundCall call))
			{
				callsByKey.Remove (key);
				callsByWhich.Remove (call.which);
			}
		}

		public bool tryGetCall (string key, out InboundCall call)
		{
			return callsByKey.TryGetValue (key, out call);
		}

		public bool tryGetCall (int which, out InboundCall call)
		{
			return callsByWhich.TryGetValue (which, out call);
		}

		public InboundCall chooseCall (Random rng)
		{
			return Utility.GetRandom (callsByKey.Values.ToList (), rng);
		}

		public IEnumerable<InboundCall> listCalls ()
		{
			return callsByKey.Values;
		}
	}
}
