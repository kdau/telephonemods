using Newtonsoft.Json;
using StardewModdingAPI;
using StardewValley;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace TelephoneFramework
{
	internal class ContentPack
	{
		[JsonIgnore]
		private IContentPack smapiPack;

#pragma warning disable IDE1006

		public List<AddOutboundCall> AddOutboundCalls { get; set; }
		public List<RemoveOutboundCall> RemoveOutboundCalls { get; set; }
		public List<AddInboundCall> AddInboundCalls { get; set; }
		public List<RemoveInboundCall> RemoveInboundCalls { get; set; }

#pragma warning restore IDE1006

		public IManifest manifest => smapiPack.Manifest;
		public string id => manifest.UniqueID;

		public string interpret (string input)
		{
			if (input == null)
				return null;

			var baseMatch = Regex.Match (input, @"^%base:(.+)$");
			if (baseMatch.Success)
				return Game1.content.LoadString (baseMatch.Groups[1].Value);

			var charMatch = Regex.Match (input, @"^%char:(.+)$");
			if (charMatch.Success)
			{
				var npc = Game1.getCharacterFromName (charMatch.Groups[1].Value);
				return npc?.displayName ?? charMatch.Groups[1].Value;
			}

			var i18nMatch = Regex.Match (input, @"^%i18n:([^%]+)(?:%([^%:]+):([^%]*))*$");
			if (i18nMatch.Success)
			{
				Dictionary<string, string> tokens = new ();
				for (int i = 2; i < i18nMatch.Groups.Count; i += 2)
					tokens[i18nMatch.Groups[i].Value] = i18nMatch.Groups[i + 1].Value;
				return smapiPack.Translation.Get (i18nMatch.Groups[1].Value, tokens);
			}

			return input;
		}

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			AddOutboundCalls ??= new ();
			AddOutboundCalls.ForEach ((call) => call.onConstruction (this));

			RemoveOutboundCalls ??= new ();
			RemoveOutboundCalls.ForEach ((call) => call.onConstruction (this));

			AddInboundCalls ??= new ();
			AddInboundCalls.ForEach ((call) => call.onConstruction (this));

			RemoveInboundCalls ??= new ();
			RemoveInboundCalls.ForEach ((call) => call.onConstruction (this));
		}

		internal void onConstruction (IContentPack smapiPack)
		{
			this.smapiPack = smapiPack;
		}
	}
}
