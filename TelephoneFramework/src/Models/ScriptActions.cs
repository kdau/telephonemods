using StardewModdingAPI;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TelephoneFramework
{
	internal class ScriptActions : List<ScriptAction>
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;

		public async Task<ActionResult> performAsync ()
		{
			foreach (var action in this)
			{
				switch (await action.performAsync ())
				{
				case ActionResult.Break:
					return ActionResult.Next;
				case ActionResult.End:
					return ActionResult.End;
				}
			}
			return ActionResult.Next;
		}

		internal virtual void onConstruction (ContentPack pack)
		{
			foreach (var action in this)
				action.onConstruction (pack);
		}
	}
}
