using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace TelephoneFramework
{
	internal class AddInboundCall : AddCallChange
	{
#pragma warning disable IDE1006

		public Conditions GlobalConditions { get; set; }
		public uint MaxTimes { get; set; } = 0;
		public Conditions PlayerConditions { get; set; }

#pragma warning restore IDE1006

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			GlobalConditions ??= new ();
			PlayerConditions ??= new ();
		}
	}
}
