using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace TelephoneFramework
{
	public enum BaseOutboundCall
	{
		Carpenter,
		Blacksmith,
		SeedShop,
		AnimalShop,
		Saloon,
		AdventureGuild,
	}

	internal class RemoveOutboundCall : CallChange
	{
#pragma warning disable IDE1006

		public BaseOutboundCall Key { get; set; }
		public Conditions Conditions { get; set; }

#pragma warning restore IDE1006

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			Conditions ??= new ();
		}
	}
}
