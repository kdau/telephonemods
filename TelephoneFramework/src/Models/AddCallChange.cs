using StardewModdingAPI;
using System;
using System.Runtime.Serialization;

namespace TelephoneFramework
{
	internal abstract class AddCallChange : CallChange
	{
#pragma warning disable IDE1006

		public string Key { get; set; }
		public ScriptActions Actions { get; set; }

#pragma warning restore IDE1006

		public string globalKey => $"{pack.id}/{Key}";

		public async void startCallActions ()
		{
			try
			{
				await Actions.performAsync ();
				await Utilities.ForCallback ((callback) =>
					Api.playHandsetUpDown (() => callback ()));
			}
			catch (Exception e)
			{
				Monitor.Log ($"Actions for call {Key} in pack {pack.id} failed:\n{e}",
					LogLevel.Error);
				Monitor.Log (e.StackTrace, LogLevel.Trace);
			}
		}

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			Actions ??= new ();
		}

		internal override void onConstruction (ContentPack pack)
		{
			base.onConstruction (pack);
			Actions.onConstruction (pack);
		}
	}
}
