using Newtonsoft.Json;
using StardewValley.Objects;
using System.Runtime.Serialization;

namespace TelephoneFramework
{
	internal class RemoveInboundCall : CallChange
	{
#pragma warning disable IDE1006

		public Phone.PhoneCalls Key { get; set; }
		public Conditions GlobalConditions { get; set; }
		public Conditions PlayerConditions { get; set; }

#pragma warning restore IDE1006

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			GlobalConditions ??= new ();
			PlayerConditions ??= new ();
		}
	}
}
