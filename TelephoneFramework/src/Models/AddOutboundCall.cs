using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace TelephoneFramework
{
	internal class AddOutboundCall : AddCallChange
	{
#pragma warning disable IDE1006

		public Conditions Conditions { get; set; }
		public string MenuText { get; set; }
		public int[] PhoneNumber { get; set; }

#pragma warning restore IDE1006

		public string menuTextInterpreted => pack.interpret (MenuText);

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			Conditions ??= new ();
		}
	}
}
