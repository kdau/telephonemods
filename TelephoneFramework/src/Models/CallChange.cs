using Newtonsoft.Json;
using StardewModdingAPI;

namespace TelephoneFramework
{
	internal abstract class CallChange
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;
		internal protected static Api Api => ModEntry.Instance.api;

		[JsonIgnore]
		public ContentPack pack { get; private set; }

		internal virtual void onConstruction (ContentPack pack)
		{
			this.pack = pack;
		}
	}
}
