using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using StardewModdingAPI;
using StardewValley;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace TelephoneFramework
{
	public enum ActionType
	{
		Dialogue,
		Event,
		Letter,
		Question,
		ShopMenu,
		When,
		Break,
		End,
	}

	public enum ActionResult
	{
		Next,
		Break,
		End,
	}

	public enum LetterRecipients
	{
		Player,
		HostPlayer,
		AllPlayers,
	}

	internal class Answer
	{
		[JsonIgnore]
		public ContentPack pack { get; private set; }

#pragma warning disable IDE1006

		public string MenuText { get; set; }
		public ScriptActions Actions { get; set; }

#pragma warning restore IDE1006

		public string menuTextInterpreted => pack.interpret (MenuText);

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			Actions ??= new ();
		}

		internal virtual void onConstruction (ContentPack pack)
		{
			this.pack = pack;
			Actions.onConstruction (pack);
		}
	}

	internal class ScriptAction
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;

		[JsonIgnore]
		public ContentPack pack { get; private set; }

#pragma warning disable IDE1006

		public ActionType Type { get; set; }
		public Conditions Conditions { get; set; }
		public List<Answer> Answers; // Type = Question
		public string Dialogue { get; set; } // Type = Dialogue | ShopMenu
		public ScriptActions ElseActions { get; set; } // Type = When
		public int ID { get; set; } = -1; // Type = Event
		public string Letter { get; set; } // Type = Letter
		public bool MailFlag { get; set; } = false; // Type = Letter
		public string NPC { get; set; } // Type = Dialogue | Question
		public string Portrait { get; set; } // Type = Dialogue | ShopMenu
		public string Question { get; set; } // Type = Question
		public bool ReadOnly { get; set; } = true; // Type = ShopMenu
		public LetterRecipients Recipients { get; set; } = LetterRecipients.Player; // Type = Letter
		public string Script { get; set; } // Type = Event
		public string Shop { get; set; } // Type = ShopMenu
		public ScriptActions ThenActions { get; set; } // Type = When
		public bool Tomorrow { get; set; } // Type = Letter

#pragma warning restore IDE1006

		public string dialogueInterpreted => pack.interpret (Dialogue);
		public string questionInterpreted => pack.interpret (Question);
		public string scriptInterpreted => pack.interpret (Script);

		public async Task<ActionResult> performAsync ()
		{
			if (!Conditions.check ())
			{
				if (Type == ActionType.When)
					return await ElseActions.performAsync ();
				else
					return ActionResult.Next;
			}

			switch (Type)
			{
			case ActionType.Dialogue:
				await performDialogueAsync ();
				break;

			case ActionType.Event:
				await performEventAsync ();
				break;

			case ActionType.Letter:
				performLetter ();
				break;

			case ActionType.Question:
				return await performQuestionAsync ();

			case ActionType.ShopMenu:
				await performShopMenuAsync ();
				break;

			case ActionType.When:
				return await ThenActions.performAsync ();

			case ActionType.Break:
				return ActionResult.Break;

			case ActionType.End:
				return ActionResult.End;

			default:
				Monitor.Log ($"Skipping action of unknown type ${Type}", LogLevel.Warn);
				break;
			}

			return ActionResult.Next;
		}

		private async Task performDialogueAsync ()
		{
			if (Dialogue == null)
			{
				Monitor.Log ($"Skipping dialogue since none was given", LogLevel.Warn);
				return;
			}

			var npc = getNPC ();
			if (npc != null)
				Game1.drawDialogue (npc, dialogueInterpreted, getPortrait ());
			else
				Game1.drawObjectDialogue (dialogueInterpreted);

			await Utilities.ForCallback ((callback) =>
				Game1.afterDialogues = callback);
		}

		private async Task performEventAsync ()
		{
			Event @event;
			if (Script != null)
			{
				@event = new Event (scriptInterpreted, ID);
			}
			else if (ID > 0)
			{
				@event = Game1.currentLocation.findEventById (ID);
				if (@event == null)
				{
					Monitor.Log ($"Skipping event ID {ID} since it could not be found and no script was given", LogLevel.Warn);
					return;
				}
			}
			else
			{
				Monitor.Log ($"Skipping event since neither an ID nor a script was given", LogLevel.Warn);
				return;
			}

			Game1.currentLocation.startEvent (@event);

			await Utilities.ForCallback ((callback) => @event.onEventFinished =
				(Action) Delegate.Combine (@event.onEventFinished, callback));
		}

		private void performLetter ()
		{
			if (Letter == null)
			{
				Monitor.Log ($"Skipping letter since no ID was given", LogLevel.Warn);
				return;
			}

			if (Recipients == LetterRecipients.HostPlayer)
			{
				if (Tomorrow)
					Game1.MasterPlayer.mailForTomorrow.Add (MailFlag ? $"${Letter}%&NL&%" : Letter);
				else if (MailFlag)
					Game1.MasterPlayer.mailReceived.Add (Letter);
				else
					Game1.MasterPlayer.mailbox.Add (Letter);
			}
			else if (Tomorrow)
			{
				Game1.addMailForTomorrow (Letter, MailFlag,
					Recipients == LetterRecipients.AllPlayers);
			}
			else
			{
				Game1.addMail (Letter, MailFlag,
					Recipients == LetterRecipients.AllPlayers);
			}
		}

		private async Task<ActionResult> performQuestionAsync ()
		{
			if (Answers.Count == 0)
			{
				Monitor.Log ($"Skipping question since no answers were given", LogLevel.Warn);
				return ActionResult.Next;
			}

			var responses = Answers.Select ((answer, i) =>
				new Response (i.ToString (), answer.menuTextInterpreted ?? "..."))
				.ToList ();
			responses.Add (new Response ("HangUp",
				Game1.content.LoadString ("Strings\\Characters:Phone_HangUp")));

			TaskCompletionSource<string> waiter = new ();
			Game1.currentLocation.createQuestionDialogue (questionInterpreted
				?? Game1.content.LoadString ("Strings\\\\Characters:Phone_SelectOption"),
				responses.ToArray (),
				(_, answerKey) => waiter.TrySetResult (answerKey),
				getNPC ());

			var answerKey = await waiter.Task;
			if (answerKey == "HangUp")
				return ActionResult.End;

			var answer = Answers[int.Parse (answerKey)];
			return await answer.Actions.performAsync ();
		}

		private async Task performShopMenuAsync ()
		{
			if (!STF.TryGetShop (Shop, out IClickableMenu shop))
			{
				Monitor.Log ($"Skipping shop menu for {Shop} since it could not be found", LogLevel.Warn);
				return;
			}

			switch (shop)
			{
			case ShopMenu shopMenu:
				shopMenu.readOnly = ReadOnly;
				if (Dialogue == "")
				{
					shopMenu.potraitPersonDialogue = null;
				}
				else if (Dialogue != null)
				{
					shopMenu.potraitPersonDialogue =
						Game1.parseText (dialogueInterpreted, Game1.dialogueFont, 304);
				}
				if (Portrait == "none")
				{
					shopMenu.portraitPerson = null;
				}
				else
				{
					Texture2D portrait = getPortrait ();
					if (portrait != null)
					{
						shopMenu.portraitPerson ??= new NPC ();
						shopMenu.portraitPerson.Portrait = portrait;
					}
				}
				break;
			case PurchaseAnimalsMenu animalsMenu:
				animalsMenu.readOnly = ReadOnly;
				break;
			case CarpenterMenu carpenterMenu:
				carpenterMenu.readOnly = ReadOnly;
				break;
			}

			Game1.activeClickableMenu = shop;

			await Utilities.ForCallback ((callback) =>
				shop.behaviorBeforeCleanup = (_) => callback ());
		}

		private NPC getNPC ()
		{
			var npc = Game1.getCharacterFromName (NPC);
			if (npc == null && NPC != null)
				Monitor.Log ($"Could not find NPC {NPC}; showing dialogue without an NPC or command/token support", LogLevel.Warn);
			return npc;
		}

		private Texture2D getPortrait ()
		{
			Texture2D portrait = null;
			try
			{
				if (Portrait != null)
					portrait = Helper.Content.Load<Texture2D> (Portrait, ContentSource.GameContent);
			}
			catch (Exception e)
			{
				Monitor.Log ($"Could not load portrait {Portrait}: {e}", LogLevel.Warn);
			}
			return portrait;
		}

		[OnDeserialized]
		private void onDeserialized (StreamingContext context)
		{
			Conditions ??= new ();
			Answers ??= new ();
			ElseActions ??= new ();
			ThenActions ??= new ();
		}

		internal virtual void onConstruction (ContentPack pack)
		{
			this.pack = pack;
			foreach (var answer in Answers)
				answer.onConstruction (pack);
			ElseActions.onConstruction (pack);
			ThenActions.onConstruction (pack);
		}
	}
}
