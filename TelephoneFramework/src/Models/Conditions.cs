using StardewModdingAPI;
using StardewValley;
using System.Collections.Generic;

namespace TelephoneFramework
{
	internal class Conditions : List<string>
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;

		public bool check ()
		{
			if (Count == 0)
				return true;
			foreach (var condition in this)
			{
				if (Game1.currentLocation.checkEventPrecondition ($"99999999/{condition}") > 0)
				{
					if (Config.VerboseLogging)
						Monitor.Log ($"    condition matched: {condition}; full set: {string.Join (" | ", this)}", LogLevel.Trace);
					return true;
				}
			}
			if (Config.VerboseLogging)
				Monitor.Log ($"    no conditions matched: {string.Join (" | ", this)}", LogLevel.Trace);
			return false;
		}
	}
}
