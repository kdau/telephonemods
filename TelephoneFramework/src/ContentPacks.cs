using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TelephoneFramework
{
	internal static class ContentPacks
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;
		private static Api Api => ModEntry.Instance.api;

		private static readonly List<ContentPack> Packs = new ();
		private static IEnumerable<AddOutboundCall> AddOutboundCalls =>
			Packs.SelectMany ((pack) => pack.AddOutboundCalls);
		private static IEnumerable<RemoveOutboundCall> RemoveOutboundCalls =>
			Packs.SelectMany ((pack) => pack.RemoveOutboundCalls);
		private static IEnumerable<AddInboundCall> AddInboundCalls =>
			Packs.SelectMany ((pack) => pack.AddInboundCalls);
		private static IEnumerable<RemoveInboundCall> RemoveInboundCalls =>
			Packs.SelectMany ((pack) => pack.RemoveInboundCalls);

		public static void OnEntry ()
		{
			Helper.Events.GameLoop.GameLaunched += (_, _) => Load ();
			Api.outboundCallChoosing += OnOutboundCallChoosing;
			Api.outboundCallPlaced += OnOutboundCallPlaced;
			Api.inboundCallChoosing += OnInboundCallChoosing;
			Api.inboundCallRinging += OnInboundCallRinging;
			Api.inboundCallAnswered += OnInboundCallAnswered;
		}

		public static void Load ()
		{
			Monitor.Log ("Loading content packs...", LogLevel.Info);
			Packs.Clear ();
			foreach (IContentPack smapiPack in Helper.ContentPacks.GetOwned ())
			{
				Monitor.Log ($"    {smapiPack.Manifest.Name} {smapiPack.Manifest.Version} by {smapiPack.Manifest.Author}",
					LogLevel.Info);
				if (!smapiPack.HasFile ("content.json"))
				{
					Monitor.Log ($"    ...skipping due to missing content.json",
						LogLevel.Warn);
					continue;
				}
				try
				{
					var pack = smapiPack.ReadJsonFile<ContentPack> ("content.json");
					pack.onConstruction (smapiPack);
					Packs.Add (pack);
				}
				catch (Exception e)
				{
					Monitor.Log ($"    ...skipping due to load error: {e.Message}",
						LogLevel.Error);
				}
			}
		}

		private static void OnOutboundCallChoosing (object _sender,
			IOutboundCallChoosingEventArgs e)
		{
			foreach (var call in AddOutboundCalls)
			{
				if (e.ignoreConditions || call.Conditions.check ())
				{
					e.addCall (call.pack.manifest, call.globalKey, call.menuTextInterpreted);
					if (Config.VerboseLogging)
					{
						Monitor.Log ($"Added {call.Key} to outbound list for {call.pack.id}",
							LogLevel.Trace);
					}
				}
			}

			foreach (var call in RemoveOutboundCalls)
			{
				if (e.ignoreConditions || call.Conditions.check ())
				{
					e.removeCall (call.pack.manifest, call.Key.ToString ());
					if (Config.VerboseLogging)
					{
						Monitor.Log ($"Removed {call.Key} from outbound list for {call.pack.id}",
							LogLevel.Trace);
					}
				}
			}
		}

		private static void OnOutboundCallPlaced (object _sender,
			IOutboundCallPlacedEventArgs e)
		{
			foreach (var call in AddOutboundCalls)
			{
				if (e.callKey == call.globalKey)
				{
					Monitor.Log ($"Outbound call {call.Key} being handled by {call.pack.id}",
						Config.VerboseLogging ? LogLevel.Debug : LogLevel.Trace);
					if (call.PhoneNumber != null)
						Api.playDialingSequence (call.PhoneNumber, call.startCallActions);
					else
						Api.playDialingSequence (call.Key, call.startCallActions);
					e.preventDefault ();
					break;
				}
			}
		}

		private static void OnInboundCallChoosing (object _sender,
			IInboundCallChoosingEventArgs e)
		{
			foreach (var call in AddInboundCalls)
			{
				if (e.ignoreConditions || call.GlobalConditions.check ())
				{
					e.addCall (call.pack.manifest, call.globalKey, call.MaxTimes);
					if (Config.VerboseLogging)
					{
						Monitor.Log ($"Added {call.Key} to inbound list for {call.pack.id}",
							LogLevel.Trace);
					}
				}
			}

			foreach (var call in RemoveInboundCalls)
			{
				if (e.ignoreConditions || call.GlobalConditions.check ())
				{
					e.removeCall (call.pack.manifest, call.Key.ToString ());
					if (Config.VerboseLogging)
					{
						Monitor.Log ($"Removed {call.Key} from inbound list for {call.pack.id}",
							LogLevel.Trace);
					}
				}
			}
		}

		private static void OnInboundCallRinging (object _sender,
			IInboundCallRingingEventArgs e)
		{
			foreach (var call in AddInboundCalls)
			{
				if (e.callKey == call.globalKey)
				{
					e.shouldRing = call.PlayerConditions.check ();
					if (Config.VerboseLogging)
					{
						Monitor.Log ($"Inbound call {call.Key} set {(e.shouldRing ? "audible" : "inaudible")} to {Game1.player.displayName} for {call.pack.id}",
							LogLevel.Trace);
					}
				}
			}

			foreach (var call in RemoveInboundCalls)
			{
				if (e.callKey == call.Key.ToString () && !call.PlayerConditions.check ())
				{
					e.shouldRing = false;
					if (Config.VerboseLogging)
					{
						Monitor.Log ($"Inbound call {call.Key} set inaudible to {Game1.player.displayName} for {call.pack.id}",
							LogLevel.Trace);
					}
				}
			}
		}

		private static void OnInboundCallAnswered (object _sender,
			IInboundCallAnsweredEventArgs e)
		{
			foreach (var call in AddOutboundCalls)
			{
				if (e.callKey == call.globalKey)
				{
					Monitor.Log ($"Inbound call {call.Key} being handled by {call.pack.id}",
						Config.VerboseLogging ? LogLevel.Debug : LogLevel.Trace);
					Api.playHandsetUpDown (call.startCallActions);
					e.preventDefault ();
					break;
				}
			}
		}
	}
}
