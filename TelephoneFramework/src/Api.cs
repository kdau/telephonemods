using StardewModdingAPI;
using StardewValley;
using System;
using System.Linq;

namespace TelephoneFramework
{
	public class Api : IApi
	{
		internal protected static IModHelper Helper => ModEntry.Instance.Helper;
		internal protected static IMonitor Monitor => ModEntry.Instance.Monitor;
		internal protected static ModConfig Config => ModConfig.Instance;

		// Events

		public event EventHandler<IOutboundCallChoosingEventArgs> outboundCallChoosing;
		public event EventHandler<IOutboundCallPlacedEventArgs> outboundCallPlaced;

		public event EventHandler<IInboundCallChoosingEventArgs> inboundCallChoosing;
		public event EventHandler<IInboundCallRingingEventArgs> inboundCallRinging;
		public event EventHandler<IInboundCallAnsweredEventArgs> inboundCallAnswered;

		// Action methods

		public void showOutboundMenu (bool ignoreConditions = false)
		{
			if (!Context.IsWorldReady)
				throw new InvalidOperationException ("The world is not ready.");
			OutboundCalling.IgnoreConditions.Value = ignoreConditions;
			Game1.game1.ShowTelephoneMenu ();
		}

		public void placeOutboundCall (string key, bool ignoreConditions = true)
		{
			if (!Context.IsWorldReady)
				throw new InvalidOperationException ("The world is not ready.");
			OutboundCalling.IgnoreConditions.Value = ignoreConditions;
			Game1.currentLocation.lastQuestionKey = "telephone";
			Game1.currentLocation.answerDialogue (new Response (key, "..."));
		}

		public void placeInboundCall (string key, bool ignoreConditions = true)
		{
			// Context checks are handled by the called method.
			InboundCalling.PlaceCall (key, ignoreConditions);
		}

		public void answerInboundCall (bool ignoreConditions = true)
		{
			// Context checks are handled by the called method.
			InboundCalling.AnswerCall (ignoreConditions);
		}

		// Sound methods

		public void playDialingSequence (string key,
			Action callback = null, bool freezePlayer = true)
		{
			playDialingSequence ($"telephone_{key}".GetHashCode (),
				callback: callback, freezePlayer: freezePlayer);
		}

		public void playDialingSequence (int randomSeed, int count = 7,
			Action callback = null, bool freezePlayer = true)
		{
			Random rng = new (randomSeed);
			int[] digits = new int[count];
			for (int i = 0; i < count; ++i)
				digits[i] = rng.Next (0, 10);
			playDialingSequence (digits, rng, callback, freezePlayer);
		}

		public void playDialingSequence (int[] digits,
			Action callback = null, bool freezePlayer = true)
		{
			// The sum of the digits isn't a great seed, but then randomizing
			// the time gaps between digits isn't a very big deal either.
			playDialingSequence (digits, new (digits.Sum ()), callback, freezePlayer);
		}

		internal void playDialingSequence (int[] digits, Random rng,
			Action callback = null, bool freezePlayer = true)
		{
			if (Config.VerboseLogging)
				Monitor.Log ($"Playing dialing sequence for number {string.Join ("", digits)}", LogLevel.Trace);

			int time = 495;

			DelayedAction.playSoundAfterDelay ("telephone_dialtone", time, pitch: 1200);
			time += 705;

			// The base game's dialing sequence uses fixed gaps between the
			// seven digits that vary between 160ms and 250ms. We mirror that
			// here with a random gap in that range.
			// Note that the digit pitches in the base game start at 800 =
			// 1200 + -4 * 100 and end at 1600 = 1200 + 4 * 100. We go one
			// higher to 1700 to offer ten digits.
			foreach (int digit in digits)
			{
				DelayedAction.playSoundAfterDelay ("telephone_buttonPush", time,
					pitch: 1200 + (Math.Max (0, Math.Min (9, digit)) - 4) * 100);
				time += rng.Next (16, 26) * 10;
			}

			// Extra gap before ringing begins.
			time += 480;

			DelayedAction.playSoundAfterDelay ("telephone_ringingInEar", time);
			time += 1800;

			// This sound runs concurrent to any callback behavior.
			DelayedAction.playSoundAfterDelay ("bigSelect", time);

			if (callback != null)
				DelayedAction.functionAfterDelay (new DelayedAction.delayedBehavior (callback), time);

			if (freezePlayer)
				Game1.player.freezePause = time;
		}

		public void playHandsetUpDown (Action callback = null,
			bool freezePlayer = true)
		{
			if (Config.VerboseLogging)
				Monitor.Log ($"Playing handset up/down", LogLevel.Trace);

			if (Game1.currentLocation != null)
				Game1.currentLocation.playSound ("openBox");
			else
				Game1.playSound ("openBox");

			if (callback != null)
				DelayedAction.functionAfterDelay (new DelayedAction.delayedBehavior (callback), 500);

			if (freezePlayer)
				Game1.player.freezePause = 500;
		}

		// Internal methods to dispatch events

		internal void dispatchOutboundCallChoosing (bool ignoreConditions,
			Action<IManifest, string, string, PlacedCallback> adder,
			Action<IManifest, string> remover)
		{
			outboundCallChoosing?.Invoke (this,
				new OutboundCallChoosingEventArgs (ignoreConditions, adder, remover));
		}

		internal void dispatchOutboundCallPlaced (string callKey, Action preventer)
		{
			outboundCallPlaced?.Invoke (this,
				new CallConnectedEventArgs (callKey, preventer));
		}

		internal void dispatchInboundCallChoosing (bool ignoreConditions,
			Action<IManifest, string, uint, ShouldRingCallback, AnsweredCallback> adder,
			Action<IManifest, string> remover)
		{
			inboundCallChoosing?.Invoke (this,
				new InboundCallChoosingEventArgs (ignoreConditions, adder, remover));
		}

		internal void dispatchInboundCallRinging (string callKey, ref bool shouldRing)
		{
			var e = new InboundCallRingingEventArgs (callKey, shouldRing);
			inboundCallRinging?.Invoke (this, e);
			shouldRing = e.shouldRing;
		}

		internal void dispatchInboundCallAnswered (string callKey, Action preventer)
		{
			inboundCallAnswered?.Invoke (this,
				new CallConnectedEventArgs (callKey, preventer));
		}
	}
}
