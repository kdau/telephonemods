using StardewModdingAPI;
using System;

namespace TelephoneFramework
{
	internal class OutboundCallChoosingEventArgs : EventArgs, IOutboundCallChoosingEventArgs
	{
		public OutboundCallChoosingEventArgs (bool ignoreConditions,
			Action<IManifest, string, string, PlacedCallback> adder,
			Action<IManifest, string> remover)
		{
			this.ignoreConditions = ignoreConditions;
			this.adder = adder;
			this.remover = remover;
		}

		public bool ignoreConditions { get; }

		private readonly Action<IManifest, string, string, PlacedCallback> adder;
		public void addCall (IManifest mod, string key, string menuText,
			PlacedCallback placedCallback = null)
		{
			adder (mod, key, menuText, placedCallback);
		}

		private readonly Action<IManifest, string> remover;
		public void removeCall (IManifest mod, string key)
		{
			remover (mod, key);
		}
	}

	internal class InboundCallChoosingEventArgs : EventArgs, IInboundCallChoosingEventArgs
	{
		public InboundCallChoosingEventArgs (bool ignoreConditions,
			Action<IManifest, string, uint, ShouldRingCallback, AnsweredCallback> adder,
			Action<IManifest, string> remover)
		{
			this.ignoreConditions = ignoreConditions;
			this.adder = adder;
			this.remover = remover;
		}

		public bool ignoreConditions { get; }

		private readonly Action<IManifest, string, uint, ShouldRingCallback, AnsweredCallback> adder;
		public void addCall (IManifest mod, string key, uint maxTimes = 1,
			ShouldRingCallback shouldRingCallback = null,
			AnsweredCallback answeredCallback = null)
		{
			adder (mod, key, maxTimes, shouldRingCallback, answeredCallback);
		}

		private readonly Action<IManifest, string> remover;
		public void removeCall (IManifest mod, string key)
		{
			remover (mod, key);
		}
	}

	internal class InboundCallRingingEventArgs : EventArgs, IInboundCallRingingEventArgs
	{
		public InboundCallRingingEventArgs (string callKey, bool shouldRing)
		{
			this.callKey = callKey;
			this.shouldRing = shouldRing;
		}

		public string callKey { get; }

		public bool shouldRing { get; set; }
	}

	internal class CallConnectedEventArgs : EventArgs, IOutboundCallPlacedEventArgs,
		IInboundCallAnsweredEventArgs
	{
		public CallConnectedEventArgs (string callKey, Action preventer)
		{
			this.callKey = callKey;
			this.preventer = preventer;
		}

		public string callKey { get; }

		private readonly Action preventer;
		public void preventDefault ()
		{
			preventer ();
		}
	}
}
