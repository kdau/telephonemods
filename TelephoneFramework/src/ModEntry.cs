using Harmony;
using PlatoTK;
using StardewModdingAPI;

namespace TelephoneFramework
{
	public class ModEntry : Mod
	{
		internal static ModEntry Instance { get; private set; }

		internal protected static ModConfig Config => ModConfig.Instance;
		internal HarmonyInstance harmony { get; private set; }
		internal IPlatoHelper platoHelper { get; private set; }

		internal readonly Api api = new ();

		public override void Entry (IModHelper helper)
		{
			// Make resources available.
			Instance = this;
			ModConfig.Load ();
			harmony = HarmonyInstance.Create (ModManifest.UniqueID);
			platoHelper = HelperExtension.GetPlatoHelper (this);

			// Set up modules.
			STF.OnEntry ();
			OutboundCalling.OnEntry ();
			InboundCalling.OnEntry ();
			ContentPacks.OnEntry ();
			ConsoleCommands.OnEntry ();
		}

		public override object GetApi ()
		{
			return api;
		}
	}
}
