using StardewValley;
using System;
using System.Threading.Tasks;

namespace TelephoneFramework
{
	internal static class Utilities
	{
		public static async Task ForCallback (Action<Game1.afterFadeFunction> binder)
		{
			TaskCompletionSource<bool> waiter = new ();
			binder (delegate
			{
				waiter.TrySetResult (true);
			});
			await waiter.Task;
		}
	}
}
