using Harmony;
using Microsoft.Xna.Framework.Audio;
using PlatoTK;
using StardewModdingAPI;
using StardewModdingAPI.Utilities;
using StardewValley;
using StardewValley.Objects;
using System;
using System.Collections.Generic;

namespace TelephoneFramework
{
	internal static class InboundCalling
	{
		private static IModHelper Helper => ModEntry.Instance.Helper;
		private static IMonitor Monitor => ModEntry.Instance.Monitor;
		private static ModConfig Config => ModConfig.Instance;
		private static HarmonyInstance Harmony => ModEntry.Instance.harmony;
		private static IPlatoHelper PlatoHelper => ModEntry.Instance.platoHelper;
		private static Api Api => ModEntry.Instance.api;

		private static InboundCallingData Data = null;
		private static readonly PerScreen<bool> IgnorePlayerConditions =
			new (() => false);

		public static void OnEntry ()
		{
			Helper.Events.GameLoop.DayStarted += OnDayStarted;

			Harmony.Patch (
				original: AccessTools.Method (typeof (Phone),
					nameof (Phone.minutesElapsed)),
				prefix: new HarmonyMethod (typeof (InboundCalling),
					nameof (InboundCalling.minutesElapsed_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (Phone),
					nameof (Phone.Ring)),
				prefix: new HarmonyMethod (typeof (InboundCalling),
					nameof (InboundCalling.Ring_Prefix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (Phone),
					nameof (Phone.CanHearCall)),
				postfix: new HarmonyMethod (typeof (InboundCalling),
					nameof (InboundCalling.CanHearCall_Postfix))
			);

			Harmony.Patch (
				original: AccessTools.Method (typeof (Phone),
					nameof (Phone.checkForAction)),
				prefix: new HarmonyMethod (typeof (InboundCalling),
					nameof (InboundCalling.checkForAction_Prefix))
			);
		}

		private static void OnDayStarted (object _sender, EventArgs _e)
		{
			// Just to be sure that nothing lingers.
			Data = null;
			IgnorePlayerConditions.Value = false;
		}

		internal static IEnumerable<InboundCall> ListCalls (bool ignoreConditions = false)
		{
			return LoadData (ignoreConditions).listCalls ();
		}

		private static InboundCallingData LoadData (bool ignoreConditions = false)
		{
			if (!Context.IsWorldReady)
				throw new InvalidOperationException ("The world is not ready.");

			InboundCallingData result = new ();

			// Let API consumers, including content packs, add
			// or remove calls.
			Api.dispatchInboundCallChoosing (ignoreConditions,
				(mod, key, maxTimes, cb1, cb2) =>
					result.addCall (key, mod.UniqueID, maxTimes, cb1, cb2),
				(_, key) => result.removeCall (key));

			return result;
		}

		internal static void PlaceCall (string key = null,
			bool ignoreConditions = false)
		{
			if (!Context.IsWorldReady)
				throw new InvalidOperationException ("The world is not ready.");
			if (!Context.IsMainPlayer)
				throw new InvalidOperationException ("Only the farmer can do that.");
			if (Phone.intervalsToRing != 0)
				throw new InvalidOperationException ("Phones may still be ringing.");

			Monitor.Log ($"Choosing an inbound call...",
				Config.VerboseLogging ? LogLevel.Info : LogLevel.Trace);

			// Start a new set of calling data.
			Data = LoadData (ignoreConditions);

			// Choose the call. The time-of-day logic for base
			// game calls is moved here to a shouldRingCallback.
			InboundCall call;
			if (key != null)
			{
				if (!Data.tryGetCall (key, out call))
					throw new KeyNotFoundException ($"Unknown inbound call key {key}.");
			}
			else
			{
				EnsureRNG ();
				call = Data.chooseCall (Phone.r);
			}
			Monitor.Log ($"...chose {call.key}",
				Config.VerboseLogging ? LogLevel.Info : LogLevel.Trace);

			// Dispatch the call to all players.
			Phone.intervalsToRing = 3;
			Game1.player.team.ringPhoneEvent.Fire (call.which);
		}

		internal static void AnswerCall (bool ignoreConditions = false)
		{
			if (!Context.IsWorldReady)
				throw new InvalidOperationException ("The world is not ready.");
			if (Phone.whichPhoneCall < 0)
				throw new InvalidOperationException ("The phone is not ringing.");
			IgnorePlayerConditions.Value = ignoreConditions;
			new Phone ().checkForAction (Game1.player, false);
		}

		private static void EnsureRNG ()
		{
			Phone.r ??= new ((int) Game1.uniqueIDForThisGame +
				(int) Game1.stats.DaysPlayed);
		}

#pragma warning disable IDE1006

		private static bool minutesElapsed_Prefix (int minutes,
			GameLocation environment, ref bool __result)
		{
			try
			{
				// Replicate the base game logic, except for the choice of call.
				if (Context.IsMainPlayer && Phone.lastMinutesElapsedTick != Game1.ticks)
				{
					Phone.lastMinutesElapsedTick = Game1.ticks;
					if (Phone.intervalsToRing == 0)
					{
						EnsureRNG ();
						if (Phone.r.NextDouble () < Config.InboundCallChance)
							PlaceCall ();
					}
					else if (--Phone.intervalsToRing <= 0)
					{
						Game1.player.team.ringPhoneEvent.Fire (-1);
					}
				}
				__result = false;
				return false;
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (minutesElapsed_Prefix)}:\n{e}",
					LogLevel.Error);
				Monitor.Log (e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		private static bool Ring_Prefix (int which_call,
			ref bool ____phoneSoundPlayed)
		{
			try
			{
				// Intercept a known call and use the extended count instead.
				if (Data != null && Data.tryGetCall (which_call, out InboundCall call))
				{
					if (call.receivedCount < call.maxTimes)
					{
						Phone.whichPhoneCall = which_call;
						Phone.ringingTimer = 0;
						____phoneSoundPlayed = false;
					}
					else if (Config.VerboseLogging)
					{
						Monitor.Log ($"Skipping inbound call from {call.key} because it has been received {call.receivedCount} times of a maximum {call.maxTimes}",
							LogLevel.Trace);
					}
					return false;
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (Ring_Prefix)}:\n{e}",
					LogLevel.Error);
				Monitor.Log (e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

		private static void CanHearCall_Postfix (int which_phone_call,
			ref bool __result)
		{
			try
			{
				if (Data != null && Data.tryGetCall (which_phone_call, out InboundCall call))
				{
					if (Config.VerboseLogging)
					{
						Monitor.Log ($"Checking whether {Game1.player.displayName} can hear inbound call from {call.key}...",
							LogLevel.Trace);
					}

					// Let API consumers, including content packs, reconsider
					// whether the call should ring.
					Api.dispatchInboundCallRinging (call.key, ref __result);

					// Let any callback reconsider it as well.
					call.shouldRingCallback?.Invoke (ref __result);

					if (Config.VerboseLogging)
					{
						Monitor.Log ($"...player {(__result ? "can" : "cannot")} hear the call",
							LogLevel.Trace);
					}
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (CanHearCall_Postfix)}:\n{e}",
					LogLevel.Error);
				Monitor.Log (e.StackTrace, LogLevel.Trace);
			}
		}


		private static bool checkForAction_Prefix (Farmer who,
			bool justCheckingForActivity, ref bool ____phoneSoundPlayed,
			ref bool __result)
		{
			try
			{
				if (!justCheckingForActivity && Data != null &&
					Data.tryGetCall (Phone.whichPhoneCall, out InboundCall call))
				{
					bool didRing = Phone.CanHearCall (call.which) ||
						IgnorePlayerConditions.Value;
					IgnorePlayerConditions.Value = false;

					bool prevented = false;

					if (didRing)
					{
						Monitor.Log ($"Handling inbound call from {call.key}",
							Config.VerboseLogging ? LogLevel.Info : LogLevel.Trace);

						// Increment the players' counter for the call if custom.
						// Base game calls are incremented below and only if
						// preventing default behavior.
						if (!call.baseGame)
							++call.receivedCount;

						// Let API consumers, including content packs, do their thing.
						Api.dispatchInboundCallAnswered (call.key,
							() =>
							{
								prevented = true;
							}
						);

						// If nothing else took over, see if a callback can.
						if (!prevented && call.answeredCallback != null)
						{
							call.answeredCallback ();
							prevented = true;
						}
					}

					// If this is a custom call, or a base game call for which
					// we are preventing default behavior, do all the same
					// cleanup as the base game method.
					if (!call.baseGame || prevented)
					{
						Monitor.Log ($"Cleaning up inbound call from {call.key}",
							Config.VerboseLogging ? LogLevel.Debug : LogLevel.Trace);

						if (____phoneSoundPlayed)
						{
							TryStopCue (Game1.soundBank.GetCue ("phone"),
								immediate: true);
							____phoneSoundPlayed = false;
						}
						if (call.baseGame && didRing)
							++call.receivedCount;
						Phone.ringingTimer = 0;
						Phone.whichPhoneCall = -1;
						__result = true;
						return false;
					}
				}
			}
			catch (Exception e)
			{
				Monitor.Log ($"Failed in {nameof (checkForAction_Prefix)}:\n{e}",
					LogLevel.Error);
				Monitor.Log (e.StackTrace, LogLevel.Trace);
			}
			return true;
		}

#pragma warning restore IDE1006

		// Tries to stop a sound cue, but uses reflection and allows for the
		// nonexistence of the method since some players have a version of the
		// game and/or XNA that doesn't like this. Also intercepts and discards
		// TypeLoadException which some players were getting that anyway.
		private static void TryStopCue (ICue cue, bool immediate = false)
		{
			try
			{
				IReflectedMethod stop =
					Helper.Reflection.GetMethod (cue, "Stop", false);
				if (stop != null)
					stop.Invoke (immediate ? AudioStopOptions.Immediate : AudioStopOptions.AsAuthored);
			}
			catch (TypeLoadException)
			{ }
		}
	}
}
