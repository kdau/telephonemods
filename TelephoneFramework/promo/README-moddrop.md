![[icon]](https://www.kdau.com/TelephoneFramework/icon.png) Reach out and touch Stardew Valley. Add inbound and outbound calls for the Telephone object with content packs or an API for SMAPI mods.

## ![Compatibility](https://www.kdau.com/headers/compatibility.png)

**Game:** Stardew Valley 1.5+

**Platform:** Linux, macOS or Windows

**Multiplayer:** works; every player must install

**Other mods:**

* The Mobile Phone mod and its phone app are completely separate from the Telephone object and this mod.
* If another mod were to alter the Telephone object's code, it would likely be incompatible with this one.

## ![Installation](https://www.kdau.com/headers/installation.png)

1. Install [SMAPI](https://smapi.io/)
1. Install [PlatoTK](https://www.nexusmods.com/stardewvalley/mods/6589) (This is different from PyTK!)
1. Install [Shop Tile Framework](https://www.moddrop.com/stardew-valley/mods/716384-shop-tile-framework) (optional, for opening shop menus)
1. Download this mod from the link in the header above
1. Unzip and put the `TelephoneFramework` folder inside your `Mods` folder
1. Run the game using SMAPI

## ![Use](https://www.kdau.com/headers/use.png)

If you are a mod user, just install this framework along with whatever mod(s) are using it and follow any instructions on those mods.

If you are a mod author, see the instructions for writing [content packs](https://gitlab.com/kdau/telephonemods/-/blob/main/TelephoneFramework/doc/CONTENT-PACKS.md) for Telephone Framework or [SMAPI mods](https://gitlab.com/kdau/telephonemods/-/blob/main/TelephoneFramework/doc/API.md) using its API.

## ![[Configuration]](https://www.kdau.com/headers/configuration.png)

If needed, you can edit this mod's configuration in its `config.json` file. It will be created in the mod's main folder (`Mods/TelephoneFramework`) the first time you run the game with the mod installed. These options are available:

* `VerboseLogging`: Set this to `true` to see much more information in the SMAPI log about inbound and outbound calls. For use in creating content packs or SMAPI mods.
* `InboundCallChance`: Set this to a probability (`0.0` = 0%, `1.0` = 100%) that the Telephone will ring with an inbound call at each interval of ten in-game minutes.

## ![Translation](https://www.kdau.com/headers/translation.png)

No translations are available yet.

This mod can be translated into any language supported by the game. Your contribution would be welcome. Please see the [instructions on the wiki](https://stardewvalleywiki.com/Modding:Translations). You can send me your work in [a GitLab issue](https://gitlab.com/kdau/telephonemods/-/issues) or the Comments tab above.

## ![Acknowledgments](https://www.kdau.com/headers/acknowledgments.png)

* Like all mods, this one is indebted to ConcernedApe, Pathoschild and the various framework modders.
* The #making-mods channel on the [Stardew Valley Discord](https://discord.gg/StardewValley) offered valuable guidance and feedback.
* In particular, [elfuun](https://www.nexusmods.com/stardewvalley/users/74620568) provided notes on the Telephone code.

## ![See also](https://www.kdau.com/headers/see-also.png)

* [Release notes](https://gitlab.com/kdau/telephonemods/-/blob/main/TelephoneFramework/doc/RELEASE-NOTES.md)
* [Source code](https://gitlab.com/kdau/telephonemods/-/tree/main/TelephoneFramework)
* [Report bugs](https://gitlab.com/kdau/telephonemods/-/issues)
* [My other Stardew stuff](https://www.kdau.com/stardew)
* Mirrors:
	[Nexus](https://www.nexusmods.com/stardewvalley/mods/TODO),
	**ModDrop**,
	[forums](https://forums.stardewvalley.net/resources/TODO/)

Other mods to consider:

* ![[icon]](https://www.kdau.com/UniversalService/icon.png) [Universal Service](https://www.moddrop.com/stardew-valley/mods/TODO) mod for calls to and from more base game characters
